## Zooniverse - Advanced Project Building

This directory contains resources for the _Advanced Project Building_ tutorial. This tutorial forms part of a series of advanced guides for managing Zoonivere projects through Python. While they can be done independently, for best usage you may want to complete them in the following order (these are also all available as Interactive Analysis workflows in the ESAP GUI):
1. [Advanced Project Building (current)](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-advanced-project-building)
2. [Advanced Aggregation with Caesar](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-advanced-aggregation-with-caesar)
3. [Integrating Machine Learning](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-integrating-machine-learning)

For guides on the basics of creating a Zooniverse project and workflow through the Zooniverse web interface, see the [Zooniverse 'Getting Started' guide](https://help.zooniverse.org/getting-started/) and ['Best Practices' guide](https://help.zooniverse.org/best-practices/). A [recorded demonstration](https://youtu.be/zJJjz5OEUAw?t=7633) and [accompanying slides](https://indico.in2p3.fr/event/21939/contributions/89045/) of using this interface are available as part of the [First ESCAPE Citizen Science Workshop](https://indico.in2p3.fr/event/21939/). For an introduction to implementing volunteer training and feedback, a [recording](https://youtu.be/zJJjz5OEUAw?t=12721) and [slides](https://indico.in2p3.fr/event/21939/contributions/89044/) are also available.

The advanced tutorial presented here includes demonstrations of using Python for:
* Creating new subject sets
* Improving the volunteer experience through extra metadata
* Retrieving a list of subject sets
* Downloading classification and subject data
* Setting up volunteer feedback and training

You can find the code for the tutorial in the `notebooks` folder and the data that were used in the `data` folder.
This tutorial makes use of example material (subjects, metadata, classifications) from the [_SuperWASP Variable Stars_](https://www.zooniverse.org/projects/ajnorton/superwasp-variable-stars) Zooniverse project, which involves classifying light curves (how brightness varies over time) of stars.

A recorded walkthrough of this advanced tutorial is available [here](https://youtu.be/o9SzgsZvOCg).

The ESAP Archives (accessible via the ESAP GUI) include data retrieval from the Zooniverse Classification Database using the ESAP Shopping Basket. For a tutorial on loading Zooniverse data from a saved shopping basket into a notebook and performing simple aggregation of the classification results, see [here](https://git.astron.nl/astron-sdc/escape-wp5/workflows/muon-hunters-example/-/tree/master) (also available as an Interactive Analyis workflow).

### Setup

#### Option 1: ESAP workflow as a remote notebook instance

You may need to install the `panoptes_client` and `pandas` packages.
```
!python -m pip install panoptes_client
!python -m pip install pandas
```

#### Option 2: Local computer

1. Install Python 3: the easiest way to do this is to download the Anaconda build from https://www.anaconda.com/download/. This will pre-install many of the packages needed for the aggregation code.
2. Open a terminal and run: `pip install panoptes-aggregation`
3. Download the [Advanced Project Building](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-advanced-project-building) tutorial into a suitable directory.

#### Option 3: Google Colab

Google Colab is a service that runs Python code in the cloud.

1. Sign into Google Drive.
2. Make a copy of the [Advanced Project Building](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-advanced-project-building) tutorial in your own Google Drive.
3. Right click the `AdvancedProjectBuilding.ipynb` file > Open with > Google Colaboratory.
    1. If this is not an option click "Connect more apps", search for "Google Colaboratory", enable it, and refresh the page.
4. Run the following in the notebook:
    1. `!pip install panoptes_aggregation --quiet` to install the needed packages (it will take a few minutes to finish), 
    2. `from google.colab import drive; drive.mount('/content/drive')` to mount Google Drive, and 
    3. `import os; os.chdir('/content/drive/MyDrive/zooniverse-advanced-project-building/')` to change the current working directory to the example folder (adjust if you have renamed the example folder).

### Other Useful Resources

Here is a list of additional resources that you may find useful when building your own Zooniverse citizen science project.
* [_Zooniverse_ website](http://zooniverse.org) - Interested in Citizen Science? Create a **free** _Zooniverse_ account, browse other projects for inspiration, contribute yourself as a citizen scientist, and build your own project.
* [Zooniverse project builder help pages](https://help.zooniverse.org) - A great resource with practical guidance, tips and advice for building great Citizen Science projects. See the ["Building a project using the project builder"](https://youtu.be/zJJjz5OEUAw?t=7633) recorded tutorial for more information.
* [_Caesar_ web interface](https://caesar.zooniverse.org) - An online interface for the _Caesar_ advanced retirement and aggregation engine. See the ["Introducing Caesar"](https://youtu.be/zJJjz5OEUAw?t=10830) recorded tutorial for tips and advice on how to use _Caesar_ to supercharge your _Zooniverse_ project.
* [The `panoptes_client` documentation](https://panoptes-python-client.readthedocs.io/en/v1.1/) - A comprehensive reference for the Panoptes Python Client.
* [The `panoptes_aggregation` documentation](https://aggregation-caesar.zooniverse.org/docs) - A comprehensive reference for the Panoptes Aggregation tool.